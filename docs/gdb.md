---
title: gdb
updated: 2023-12-29 20:13:32+0800
created: 2023-12-29 20:13:32+0800
weather: "Qingdao: 🌦   +3°C"
latitude: 36.0649
longitude: 120.3804
---


# offsetof struct memeber
```
export PATH=/usr/local/mips/mipsel-buildroot-linux-gnu/bin:$PATH
mipsel-linux-gdb -q hichannel.ko
Reading symbols from hichannel.ko...

>>> ptype /o hcc_handler_stru
type = struct {
/*      0      |       4 */    hi_void *hi_channel;
/*      4      |      16 */    oal_mutex_stru tx_transfer_lock;
/*     20      |     944 */    hcc_transfer_handler_stru hcc_transer_info;
/*    964      |      24 */    hcc_tx_assem_descr tx_descr_info;
/*    988      |       4 */    struct hcc_bus_adpta_ops *hcc_bus_ops;

                               /* total size (bytes):  992 */
                             }
```



