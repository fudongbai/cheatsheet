---
title: vim
updated: 2023-12-29 20:19:18+0800
created: 2023-12-29 20:19:18+0800
weather: "Qingdao: 🌦   +3°C"
latitude: 36.0649
longitude: 120.3804
---


```
# Delete blank lines
# https://stackoverflow.com/a/706083
:g/^$/d
```


```
# Convert all text to lower/upper case
# https://stackoverflow.com/a/1103008
ggVGu/ggVGU
```


```
# Join every second line
# https://superuser.com/a/168967
qqJjq
500@q

# https://superuser.com/a/168993
:%normal J
:%norm J
```
